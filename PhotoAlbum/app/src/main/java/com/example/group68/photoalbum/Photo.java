package com.example.group68.photoalbum;

import java.util.ArrayList;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.provider.MediaStore;

public class Photo{
    String filePath;
    ArrayList<Tag> tagList;

    Photo(String filePath){
        this.filePath = filePath;
        tagList = new ArrayList<Tag>();
    }

    void addTag(Tag t){
        tagList.add(t);
    }

    public String getTagString(){
        String s = "";
        Tag t;
        for(int i = 0; i < tagList.size(); i++){
            t = tagList.get(i);

            if(t.type == 0){
                s += "Person: ";
            }
            else{
                s += "Location: ";
            }
            s += t.value;

            if(i < tagList.size() - 1){
                s += ", ";
            }
        }

        return s;
    }

    public ArrayList<String> getFullList(){
        ArrayList<String> tags = new ArrayList<String>();

        for(Tag t: tagList){
            tags.add(t.toString());
        }

        return tags;
    }

    public ArrayList<String> getPersonList(){
        ArrayList<String> tags = new ArrayList<String>();

        for(Tag t: tagList){
            if(t.type == 0){
                tags.add(t.value);
            }
        }

        return tags;
    }

    public ArrayList<String> getLocationList(){
        ArrayList<String> tags = new ArrayList<String>();

        for(Tag t: tagList){
            if(t.type == 1){
                tags.add(t.value);
            }
        }

        return tags;
    }

    public String toString(){
        String s = filePath;
        for(Tag t: tagList){
            s+= "\n#" + t.type + t.value;
        }
        return s;
    }
}