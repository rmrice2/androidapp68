package com.example.group68.photoalbum;

/**
 * Created by Denny on 4/28/2016.
 */
import java.io.*;

import android.app.Activity;
import android.content.ContentResolver;
import android.content.Context;
import android.content.CursorLoader;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.GridView;
import android.widget.Toast;

public class OpenAlbum extends AppCompatActivity {

    public static final String ALBUM_NAME = "album_name";
    public static final String CURRENT_PHOTO = "photo";
    public static final int ADD_PHOTO_CODE = 1;
    public static final int DISPLAY_PHOTO_CODE = 0;

    private GridView gridView;
    private GridViewAdapter gridAdapter;

    int maxItem; //total amount of photo items in this album
    int currItem; //index of current photo item
    int currentAlbum;
    private AlbumList albumList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.open_album);

        Toolbar toolbar = (Toolbar)findViewById(R.id.my_toolbar);
        setSupportActionBar(toolbar);

        ActionBar ab = getSupportActionBar();
        ab.setDisplayHomeAsUpEnabled(true);

        maxItem = 0;
        currItem = 0;
        currentAlbum = -1;
        try {
            albumList = AlbumList.getInstance(this);
            albumList.setContext(this);
        } catch (IOException e) {
            e.printStackTrace();
        }

        // check if Bundle was passed, and populate fields
        Bundle bundle = getIntent().getExtras();
        if(bundle != null) {
            String albumName = bundle.getString(ALBUM_NAME);
            currentAlbum = bundle.getInt(PhotoAlbum.ARRAY_INDEX_KEY);

            setTitle(albumName);

            //load albumlist.dat into albumList
            try {
                albumList = AlbumList.getInstance(this);
            } catch (IOException e) {
                e.printStackTrace();
            }

            gridView = (GridView) findViewById(R.id.gridView);
            gridAdapter = new GridViewAdapter(this, R.layout.photo, albumList.get(currentAlbum));
            gridView.setAdapter(gridAdapter);

            gridView.setOnItemClickListener(new OnItemClickListener() {
                public void onItemClick(AdapterView<?> parent, View v, int position, long id) {
                    displayPhoto(position);
                }
            });
        }

        if(!isExternalStorageReadable()) {
            Toast.makeText(this, "Unable to read external storage", Toast.LENGTH_LONG)
                    .show();
            finish();
        }

    }

    public void displayPhoto(int pos){
        Intent intent = new Intent(this, DisplayPhoto.class);

        //send current album index and current photo index to display photo screen
        Bundle bundle = new Bundle();
        bundle.putInt("albumIndex", currentAlbum);
        bundle.putInt("photoIndex", pos);
        intent.putExtras(bundle);

        startActivityForResult(intent, DISPLAY_PHOTO_CODE);
    }

    public void addPhoto(){
        Intent intent = new Intent(Intent.ACTION_PICK,
                android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        startActivityForResult(intent, ADD_PHOTO_CODE);
    }

    protected void onActivityResult(int requestCode, int resultCode, Intent intent){

        //called if the user pressed the back button in the display photo screen
        if(requestCode == DISPLAY_PHOTO_CODE && resultCode == Activity.RESULT_OK){
            Bundle bundle = intent.getExtras();
            currentAlbum = bundle.getInt(PhotoAlbum.ARRAY_INDEX_KEY);

            gridAdapter = new GridViewAdapter(this, R.layout.photo, albumList.get(currentAlbum));
            gridView.setAdapter(gridAdapter);
        }

        //called if the user pressed the delete button in the display photo screen
        else if(resultCode == DisplayPhoto.DELETE_PHOTO_CODE){
            Bundle bundle = intent.getExtras();
            currentAlbum = bundle.getInt(PhotoAlbum.ARRAY_INDEX_KEY);
            int currentPhoto = bundle.getInt(CURRENT_PHOTO);

            albumList.get(currentAlbum).remove(currentPhoto);

            gridAdapter = new GridViewAdapter(this, R.layout.photo, albumList.get(currentAlbum));
            gridView.setAdapter(gridAdapter);

            albumList.store();
        }

        //called if the user chose a picture from the media gallery
        else if(requestCode == ADD_PHOTO_CODE && resultCode == Activity.RESULT_OK){
            Uri uri;
            if(intent != null){
                uri = intent.getData();
                String path;

                String[] projection = {MediaStore.MediaColumns.DATA};

                ContentResolver cr = getApplicationContext().getContentResolver();
                Cursor metaCursor = cr.query(uri, projection, null, null, null);
                if (metaCursor != null) {
                    try {
                        if (metaCursor.moveToFirst()) {
                            path = metaCursor.getString(0);
                            Photo p = new Photo(path);
                            albumList.get(currentAlbum).add(p);

                            gridAdapter = new GridViewAdapter(this, R.layout.photo, albumList.get(currentAlbum));
                            gridView.setAdapter(gridAdapter);

                            albumList.store();
                        }
                    } finally {
                        metaCursor.close();
                    }
                }
            }
        }
    }

    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.open_album_menu, menu);
        return super.onCreateOptionsMenu(menu);
    }

    public boolean onOptionsItemSelected(MenuItem item) {
        switch(item.getItemId()) {
            case R.id.action_add:
                addPhoto();
                return true;

            case android.R.id.home:
                setResult(Activity.RESULT_CANCELED);
                finish();
                return true;

            default:
                return super.onOptionsItemSelected(item);
        }
    }

    public boolean isExternalStorageReadable() {
        String state = Environment.getExternalStorageState();
        if (Environment.MEDIA_MOUNTED.equals(state) ||
                Environment.MEDIA_MOUNTED_READ_ONLY.equals(state)) {
            return true;
        }
        return false;
    }

    private String getRealPathFromURI(Uri contentUri) {
        String[] proj = { MediaStore.Images.Media.DATA };
        CursorLoader loader = new CursorLoader(this, contentUri, proj, null, null, null);
        Cursor cursor = loader.loadInBackground();
        int column_index = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
        cursor.moveToFirst();
        String result = cursor.getString(column_index);
        cursor.close();
        return result;
    }
}
