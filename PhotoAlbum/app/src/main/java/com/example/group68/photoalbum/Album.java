package com.example.group68.photoalbum;

import java.util.ArrayList;

/**
 * Created by Denny on 4/26/2016.
 */
public class Album extends ArrayList<Photo> implements Comparable<Album> {

    public String name;

    public Album(String name) {
        this.name = name;
    }

    public void addPhoto(Photo p){
        this.add(p);
    }

    public String saveString(){
        String s = "$" + name;
        for(Photo p: this){
            s += "\n*" + p.toString();
        }

        return s;
    }

    public String toString(){
        return name;
    }

    public int compareTo(Album other) {
        return name.compareToIgnoreCase(other.name);
    }
}
