package com.example.group68.photoalbum;

import android.app.DialogFragment;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.EditText;

import java.util.ArrayList;


public class AddAlbum extends AppCompatActivity {

    public static final String ALBUM_NAME = "albumName";
    public static final String ALBUM_LIST = "nameList";

    EditText albumName;
    int arrayIndex;
    ArrayList<String> nameList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.add_edit_delete_album);

        Toolbar toolbar = (Toolbar)findViewById(R.id.my_toolbar);
        setSupportActionBar(toolbar);

        ActionBar ab = getSupportActionBar();
        ab.setDisplayHomeAsUpEnabled(true);

        albumName = (EditText)findViewById(R.id.album_name);
        arrayIndex = -1;
        nameList = new ArrayList<String>();

        // check if Bundle was passed, and populate fields
        Bundle bundle = getIntent().getExtras();
        if(bundle != null) {
            nameList = bundle.getStringArrayList(ALBUM_LIST);
            arrayIndex = bundle.getInt(PhotoAlbum.ARRAY_INDEX_KEY);

            if(arrayIndex != -1){
                albumName.setText(nameList.get(arrayIndex));
            }
        }
    }

    //called when the user taps the Ok button
    public void Ok(View view) {

        String name = albumName.getText().toString();

        if(name == null || name.length() == 0) {
            DialogFragment newFragment = new AlbumInfoDialogFragment();
            Bundle bundle = new Bundle();
            bundle.putString(AlbumInfoDialogFragment.MESSAGE_KEY,
                    "Album name is required");
            newFragment.setArguments(bundle);
            newFragment.show(getFragmentManager(),"empty field");
            return;   // does not quit activity, just returns from method
        }

        if(nameList != null && nameList.size() > 0) {
            for(String album : nameList) {
                if(album.compareToIgnoreCase(name) == 0) {
                    DialogFragment newFragment = new AlbumInfoDialogFragment();
                    Bundle bundle = new Bundle();
                    bundle.putString(AlbumInfoDialogFragment.MESSAGE_KEY,
                            "Album already exists");
                    newFragment.setArguments(bundle);
                    newFragment.show(getFragmentManager(),"invalid field");
                    return;   // does not quit activity, just returns from method
                }
            }
        }

        // make Bundle
        Bundle bundle = new Bundle();
        bundle.putString(ALBUM_NAME, name);
        bundle.putInt(PhotoAlbum.ARRAY_INDEX_KEY, arrayIndex);

        // send back to caller
        Intent intent = new Intent();
        intent.putExtras(bundle);

        setResult(RESULT_OK,intent);
        finish(); // pops the activity from the call stack, returns to parent
    }

    // called when the user taps the Cancel button
    public void Cancel(View view) {
        setResult(RESULT_CANCELED);
        finish();
    }
}
