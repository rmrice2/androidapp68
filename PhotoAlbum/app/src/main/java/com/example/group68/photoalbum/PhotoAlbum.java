package com.example.group68.photoalbum;

import android.Manifest;
import android.app.Activity;
import android.app.SearchManager;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.support.v4.app.ActivityCompat;
import android.support.v4.view.MenuItemCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.GridView;
import android.widget.Toast;

import java.io.*;
import java.util.ArrayList;

public class PhotoAlbum extends AppCompatActivity {

    // Storage Permissions
    private static final int REQUEST_EXTERNAL_STORAGE = 1;
    private static String[] PERMISSIONS_STORAGE = {
            Manifest.permission.READ_EXTERNAL_STORAGE,
            Manifest.permission.WRITE_EXTERNAL_STORAGE
    };

    private AlbumList albumList;
    private ArrayList<String> nameList;
    private ArrayList<Photo> results;
    private int currentSelectedIndex;
    private boolean searchOpen = false;

    private GridView gridView;
    private GridViewAdapter gridAdapter;
    private GridViewAdapter searchAdapter;
    private MenuItem searchMenuItem;

    public static final String ARRAY_INDEX_KEY = "index";
    public static final int ADD_ALBUM_CODE = 0;
    public static final int RENAME_ALBUM_CODE = 1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.album_list);

        Toolbar myToolbar = (Toolbar) findViewById(R.id.my_toolbar);
        setSupportActionBar(myToolbar);

        searchOpen = false;
        results = new ArrayList<>();
        PhotoAlbum.verifyStoragePermissions(this);

        currentSelectedIndex = -1;
        try {
            albumList = AlbumList.getInstance(this);
            albumList.setContext(this);
        } catch (IOException e) {
            Toast.makeText(this, "Error loading albums", Toast.LENGTH_LONG)
                    .show();
        }

        nameList = new ArrayList<String>();

        //populate albumList with albumlist.dat
        nameList = albumList.getAlbumNameString();

        gridView = (GridView) findViewById(R.id.album_list);
        handleIntent(getIntent());
    }

    public void showAlbumList() {
        results.clear();
        gridView.setAdapter(new ArrayAdapter<Album>(this, R.layout.album, albumList));

        gridView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                currentSelectedIndex = i;
            }});

        gridView.setNumColumns(1);
    }

    public void clearSearchResults(){
        results.clear();
        searchAdapter = new GridViewAdapter(this, R.layout.photo, results);
        gridView.setAdapter(searchAdapter);
    }

    public void showSearchResults(String query){
        int[] matches = albumList.search(query);
        if(matches == null || matches.length == 0){
            results.clear();
            searchAdapter = new GridViewAdapter(this, R.layout.photo, results);
            gridView.setAdapter(searchAdapter);
            return;
        }

        makeResultList(matches);
    }

    public void makeResultList(int[] matches){
        ArrayList<Photo> photos = albumList.getFlatList();
        results.clear();

        for(int i = 0; i < matches.length; i++){
            results.add(photos.get(matches[i]));
        }

        searchAdapter = new GridViewAdapter(this, R.layout.photo, results);
        gridView.setAdapter(searchAdapter);
        gridView.setNumColumns(3);

    }

    public void openAlbum() {
        if(currentSelectedIndex == -1){
            Toast.makeText(this, "No album selected", Toast.LENGTH_SHORT)
                    .show();
            return;
        }

        String albumName = albumList.get(currentSelectedIndex).name;

        Intent intent = new Intent(this, OpenAlbum.class);

        //send album name and albumlist index to open album screen
        Bundle bundle = new Bundle();
        bundle.putString(OpenAlbum.ALBUM_NAME, albumName);
        bundle.putInt(ARRAY_INDEX_KEY, currentSelectedIndex);
        intent.putExtras(bundle);

        startActivity(intent);
    }

    public void addAlbum() {
        Intent intent = new Intent(this, AddAlbum.class);

        if(albumList != null && albumList.size() > 0) {
            nameList = albumList.getAlbumNameString();

            Bundle bundle = new Bundle();
            bundle.putStringArrayList(AddAlbum.ALBUM_LIST, nameList);
            bundle.putInt(ARRAY_INDEX_KEY, -1);
            intent.putExtras(bundle);
        }

        startActivityForResult(intent, ADD_ALBUM_CODE);
    }

    public void renameAlbum() {
        if(currentSelectedIndex == -1){
            Toast.makeText(this, "No album selected", Toast.LENGTH_SHORT)
                    .show();
            return;
        }

        Intent intent = new Intent(this, AddAlbum.class);
        if(albumList != null && albumList.size() > 0) {
            nameList = albumList.getAlbumNameString();

            Bundle bundle = new Bundle();
            bundle.putStringArrayList(AddAlbum.ALBUM_LIST, nameList);
            bundle.putInt(ARRAY_INDEX_KEY, currentSelectedIndex);
            intent.putExtras(bundle);
        }

        startActivityForResult(intent, RENAME_ALBUM_CODE);
    }

    public void deleteAlbum() {
        if(currentSelectedIndex == -1){
            Toast.makeText(this, "No album selected", Toast.LENGTH_SHORT)
                    .show();
            return;
        }

        albumList.remove(currentSelectedIndex);

        gridView.setAdapter(new ArrayAdapter<Album>(this, R.layout.album, albumList));

        currentSelectedIndex = -1;

        //store albums to albumlist.dat
        albumList.store();
    }

    protected void onActivityResult(int requestCode, int resultCode, Intent intent) {

        if(searchOpen){
            searchMenuItem.collapseActionView();
            searchOpen = false;
        }
        if (resultCode != RESULT_OK) {
            currentSelectedIndex = -1;
            showAlbumList();
            return;
        }

        Bundle bundle = intent.getExtras();
        if (bundle == null) { return; }

        if(requestCode == ADD_ALBUM_CODE) {
            String name = bundle.getString(AddAlbum.ALBUM_NAME);
            Album a = new Album(name);

            albumList.addAlbum(name);

            currentSelectedIndex = -1;
            showAlbumList();
            //gridView.setAdapter(new ArrayAdapter<Album>(this, R.layout.album, albumList));
        }

        else if(requestCode == RENAME_ALBUM_CODE){
            String name = bundle.getString(AddAlbum.ALBUM_NAME);
            int index = bundle.getInt(ARRAY_INDEX_KEY);

            albumList.get(index).name = name;

            //try to store albums to albumlist.dat

            currentSelectedIndex = -1;
            gridView.setAdapter(new ArrayAdapter<Album>(this, R.layout.album, albumList));
            albumList.store();
        }
    }
	
	public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.my_menu, menu);

        searchMenuItem = menu.findItem(R.id.action_search);
        MenuItemCompat.setOnActionExpandListener(searchMenuItem,new MenuItemCompat.OnActionExpandListener() {

            @Override
            public boolean onMenuItemActionExpand(MenuItem item) {
                searchOpen = true;
                currentSelectedIndex = -1;

                clearSearchResults();

                return true;
            }

            @Override
            public boolean onMenuItemActionCollapse(MenuItem item) {
                searchOpen = false;
                showAlbumList();
                return true;
            }
        });

        // Get the SearchView and set the searchable configuration
        SearchManager searchManager = (SearchManager) getSystemService(Context.SEARCH_SERVICE);
        SearchView searchView = (SearchView) searchMenuItem.getActionView();
        // Assumes current activity is the searchable activity
        searchView.setSearchableInfo(searchManager.getSearchableInfo(getComponentName()));
        // Do not iconify the widget; expand it by default
        searchView.setIconifiedByDefault(false);

        SearchView.OnQueryTextListener textChangeListener = new SearchView.OnQueryTextListener(){
            @Override
            public boolean onQueryTextChange(String newText)
            {
                // this is your adapter that will be filtered
                if(newText.length() > 0)
                {
                    showSearchResults(newText);
                }
                return true;
            }
            @Override
            public boolean onQueryTextSubmit(String query)
            {
                // this is your adapter that will be filtered
                if(query.length() > 0)
                {
                    showSearchResults(query);
                }
                return true;
            }
        };

        searchView.setOnQueryTextListener(textChangeListener);

        return super.onCreateOptionsMenu(menu);
    }

    protected void onNewIntent(Intent intent) {
        setIntent(intent);
        handleIntent(intent);
    }

    private void handleIntent(Intent intent) {
        if (searchOpen && Intent.ACTION_SEARCH.equals(intent.getAction())) {
            String query = intent.getStringExtra(SearchManager.QUERY);
            showSearchResults(query);
        } else {
            showAlbumList();
        }
    }

    public boolean onOptionsItemSelected(MenuItem item) {
        switch(item.getItemId()) {
            case R.id.action_add:
                addAlbum();
                return true;

            case R.id.action_edit:
                if(searchOpen){
                    Toast.makeText(this, "No album selected", Toast.LENGTH_SHORT)
                            .show();
                    return false;
                }
                renameAlbum();
                return true;

            case R.id.action_delete:
                if(searchOpen){
                    Toast.makeText(this, "No album selected", Toast.LENGTH_SHORT)
                            .show();
                    return false;
                }
                deleteAlbum();
                return true;

            case R.id.action_open:
                if(searchOpen){
                    Toast.makeText(this, "No album selected", Toast.LENGTH_SHORT)
                            .show();
                    return false;
                }
                openAlbum();
                return true;

            default:
                return super.onOptionsItemSelected(item);
        }
    }

    /**
     * Checks if the app has permission to write to device storage
     *
     * If the app does not has permission then the user will be prompted to grant permissions
     *
     * @param activity
     */
    public static void verifyStoragePermissions(Activity activity) {
        // Check if we have write permission
        int permission = ActivityCompat.checkSelfPermission(
                activity, Manifest.permission.WRITE_EXTERNAL_STORAGE);

        if (permission != PackageManager.PERMISSION_GRANTED) {
            // We don't have permission so prompt the user
            ActivityCompat.requestPermissions(
                    activity,
                    PERMISSIONS_STORAGE,
                    REQUEST_EXTERNAL_STORAGE
            );
        }
    }
}
