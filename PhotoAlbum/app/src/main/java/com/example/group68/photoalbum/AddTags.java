package com.example.group68.photoalbum;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.RadioGroup;
import android.widget.RadioGroup.OnCheckedChangeListener;
import android.widget.Toast;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.util.ArrayList;

/**
 * Created by Becca on 5/1/16.
 */
public class AddTags extends AppCompatActivity {

    public static final String TAG_TYPE = "type";
    public static final String TAG_VALUE = "value";
    int currentAlbum;
    int currentPhoto;
    int selectedType = -1;
    ArrayList<String> people;
    ArrayList<String> places;

    private EditText valueText;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.add_tags);

        Toolbar toolbar = (Toolbar)findViewById(R.id.my_toolbar);
        setSupportActionBar(toolbar);

        ActionBar ab = getSupportActionBar();
        ab.setDisplayHomeAsUpEnabled(true);

        Bundle bundle = getIntent().getExtras();
        currentAlbum = bundle.getInt(PhotoAlbum.ARRAY_INDEX_KEY);
        currentPhoto = bundle.getInt(OpenAlbum.CURRENT_PHOTO);
        people = bundle.getStringArrayList(DisplayPhoto.PERSON_LIST);
        places = bundle.getStringArrayList(DisplayPhoto.LOCATION_LIST);

        RadioGroup radioGroup = (RadioGroup) findViewById(R.id.type_selection);
        radioGroup.setOnCheckedChangeListener(new OnCheckedChangeListener()
        {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedID) {
                if(checkedID == R.id.person_button){
                    selectedType = 0;
                }
                else{
                    selectedType = 1;
                }
            }
        });

        valueText = (EditText)findViewById(R.id.tag_value);
    }

    public void addTag(View view){
        if(selectedType == -1 || valueText.getText() == null || valueText.getText().length() == 0){
            Toast.makeText(this, "Please choose a tag type and enter a value", Toast.LENGTH_SHORT)
                    .show();
            return;
        }

        String value = valueText.getText().toString();
        //Photo p = albumList.get(currentAlbum).get(currentPhoto);

        if(selectedType == 0){
            for(String person: people){
                if(person.equalsIgnoreCase(value)){
                    Toast.makeText(this, "Person tag already present", Toast.LENGTH_SHORT)
                            .show();
                    return;
                }
            }
        }
        else{
            for(String place: places){
                if(place.equalsIgnoreCase(value)){
                    Toast.makeText(this, "Location tag already present", Toast.LENGTH_SHORT)
                            .show();
                    return;
                }
            }
        }

        valueText.setText("");

        // make Bundle
        Bundle bundle = new Bundle();

        bundle.putInt(PhotoAlbum.ARRAY_INDEX_KEY, currentAlbum);
        bundle.putInt(OpenAlbum.CURRENT_PHOTO, currentPhoto);
        bundle.putInt(TAG_TYPE, selectedType);
        bundle.putString(TAG_VALUE, value);

        // send back to caller
        Intent intent = new Intent();
        intent.putExtras(bundle);

        setResult(RESULT_OK, intent);
        finish(); // pops the activity from the call stack, returns to parent
    }

    public boolean onOptionsItemSelected(MenuItem item) {
        switch(item.getItemId()) {

            //if the back button is pressed, cancel activity
            case android.R.id.home:
                setResult(Activity.RESULT_CANCELED);
                finish();
                return true;

            default:
                return super.onOptionsItemSelected(item);
        }
    }
}
