package com.example.group68.photoalbum;

/**
 * Created by Becca on 4/30/2016.
 */
public class Tag{
    int type;
    String value;

    Tag(int type, String value){
        this.type = type;
        this.value = value;
    }

    public String toString(){
        if(type == 0){
            return "Person: " + value;
        }
        else{
            return "Location: " + value;
        }
    }
}
