package com.example.group68.photoalbum;

import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.app.DialogFragment;
import android.support.v7.app.AlertDialog;

/**
 * Created by Denny on 4/26/2016.
 */
public class AlbumInfoDialogFragment extends DialogFragment {

    public static final String MESSAGE_KEY = "dialog_message";

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        // Use the Builder class for convenient dialog construction
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());

        // get the message
        Bundle bundle = getArguments();

        builder.setMessage(bundle.getString(MESSAGE_KEY))
                .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        // FIRE ZE MISSILES!
                    }
                });

        // Create the AlertDialog object and return it
        return builder.create();
    }
}
