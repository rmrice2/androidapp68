package com.example.group68.photoalbum;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import java.io.*;
import java.util.ArrayList;

public class DisplayPhoto extends AppCompatActivity {

    int currentAlbum;
    int currentPhoto;
    AlbumList albumList;
    Photo photo;
    private ImageView imageView;
    private TextView caption;
    private TextView tags;

    public static final int ADD_TAGS_CODE = 0;
    public static final int MOVE_PHOTO_CODE = 1;
    public static final int DELETE_PHOTO_CODE = 2;
    public static final int DELETE_TAGS_CODE = 3;
    public static final String PERSON_LIST = "people";
    public static final String LOCATION_LIST = "places";
    public static final String FULL_LIST = "full";
    public static final String ALBUM_LIST = "albums";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.display_photo);

        Toolbar toolbar = (Toolbar)findViewById(R.id.my_toolbar);
        setSupportActionBar(toolbar);

        ActionBar ab = getSupportActionBar();
        ab.setDisplayHomeAsUpEnabled(true);

        Bundle bundle = getIntent().getExtras();
        currentAlbum = bundle.getInt("albumIndex");
        currentPhoto = bundle.getInt("photoIndex");

        try {
            albumList = AlbumList.getInstance(this);
        } catch (IOException e) {
            e.printStackTrace();
        }

        photo = albumList.get(currentAlbum).get(currentPhoto);
        setTitle(albumList.get(currentAlbum).name);

        display();
    }

    public void display() {
        imageView = (ImageView) findViewById(R.id.image);
        imageView.setImageURI(Uri.parse(photo.filePath));

        caption = (TextView)findViewById(R.id.caption);
        caption.setText(photo.filePath);
        tags = (TextView)findViewById(R.id.tags);
        tags.setText(photo.getTagString());
    }

    public void prev(View view) {
        if(currentPhoto == 0){
            currentPhoto = albumList.get(currentAlbum).size() - 1;
        }
        else{
            currentPhoto--;
        }

        photo = albumList.get(currentAlbum).get(currentPhoto);
        imageView.setImageURI(Uri.parse(photo.filePath));
        caption.setText(photo.filePath);
        tags.setText(photo.getTagString());
    }

    public void next(View view) {
        int albumSize = albumList.get(currentAlbum).size();

        if(currentPhoto == albumSize - 1){
            currentPhoto=0;
        }
        else{
            currentPhoto++;
        }

        photo = albumList.get(currentAlbum).get(currentPhoto);
        imageView.setImageURI(Uri.parse(photo.filePath));
        caption.setText(photo.filePath);
        tags.setText(photo.getTagString());
    }

    public void deletePhoto(){
        Intent delete = new Intent();
        Bundle deleteBundle = new Bundle();
        deleteBundle.putInt(PhotoAlbum.ARRAY_INDEX_KEY, currentAlbum);
        deleteBundle.putInt(OpenAlbum.CURRENT_PHOTO, currentPhoto);
        delete.putExtras(deleteBundle);
        setResult(DELETE_PHOTO_CODE, delete);
        finish();
    }

    public void addTags(){

        Intent intent = new Intent(this, AddTags.class);

        Bundle bundle = new Bundle();
        bundle.putStringArrayList(PERSON_LIST, photo.getPersonList());
        bundle.putStringArrayList(LOCATION_LIST, photo.getLocationList());
        bundle.putInt(PhotoAlbum.ARRAY_INDEX_KEY, currentAlbum);
        bundle.putInt(OpenAlbum.CURRENT_PHOTO, currentPhoto);
        intent.putExtras(bundle);

        startActivityForResult(intent, ADD_TAGS_CODE);
    }

    public void deleteTags(){
        if(photo.tagList.size() == 0){
            Toast.makeText(this, "No tags", Toast.LENGTH_SHORT)
                    .show();
            return;
        }

        Intent intent = new Intent(this, DeleteTags.class);

        Bundle bundle = new Bundle();
        bundle.putStringArrayList(FULL_LIST, photo.getFullList());
        bundle.putInt(PhotoAlbum.ARRAY_INDEX_KEY, currentAlbum);
        bundle.putInt(OpenAlbum.CURRENT_PHOTO, currentPhoto);
        intent.putExtras(bundle);

        startActivityForResult(intent, DELETE_TAGS_CODE);
    }

    public void movePhoto(){
        if(albumList.size() == 1){
            Toast.makeText(this, "Only one album exists", Toast.LENGTH_SHORT)
                    .show();
            return;
        }

        Intent intent = new Intent(this, MovePhoto.class);

        Bundle bundle = new Bundle();
        bundle.putStringArrayList(ALBUM_LIST, albumList.getAlbumNameString());
        bundle.putInt(PhotoAlbum.ARRAY_INDEX_KEY, currentAlbum);
        bundle.putInt(OpenAlbum.CURRENT_PHOTO, currentPhoto);
        intent.putExtras(bundle);

        startActivityForResult(intent, MOVE_PHOTO_CODE);
    }

    protected void onActivityResult(int requestCode, int resultCode, Intent intent) {

        if(requestCode == ADD_TAGS_CODE && resultCode == RESULT_OK){
            Bundle bundle = intent.getExtras();

            currentAlbum = bundle.getInt(PhotoAlbum.ARRAY_INDEX_KEY);
            currentPhoto = bundle.getInt(OpenAlbum.CURRENT_PHOTO);

            Tag t = new Tag(bundle.getInt(AddTags.TAG_TYPE), bundle.getString(AddTags.TAG_VALUE));

            try {
                albumList = AlbumList.getInstance(this);
                albumList.setContext(this);
            } catch (IOException e) {
                e.printStackTrace();
            }

            photo = albumList.get(currentAlbum).get(currentPhoto);
            photo.addTag(t);

            albumList.store();

            display();
        }
        else if(requestCode == DELETE_TAGS_CODE && resultCode == RESULT_OK){
            Bundle bundle = intent.getExtras();

            currentAlbum = bundle.getInt(PhotoAlbum.ARRAY_INDEX_KEY);
            currentPhoto = bundle.getInt(OpenAlbum.CURRENT_PHOTO);

            try {
                albumList = AlbumList.getInstance(this);
                albumList.setContext(this);
            } catch (IOException e) {
                e.printStackTrace();
            }

            photo = albumList.get(currentAlbum).get(currentPhoto);
            int i = bundle.getInt(DeleteTags.TAG_INDEX);
            photo.tagList.remove(i);
            
            albumList.store();

            display();
        }
        else if(requestCode == MOVE_PHOTO_CODE && resultCode == RESULT_OK){
            Bundle bundle = intent.getExtras();

            currentAlbum = bundle.getInt(PhotoAlbum.ARRAY_INDEX_KEY);
            currentPhoto = bundle.getInt(OpenAlbum.CURRENT_PHOTO);

            try {
                albumList = AlbumList.getInstance(this);
                albumList.setContext(this);
            } catch (IOException e) {
                e.printStackTrace();
            }

            photo = albumList.get(currentAlbum).get(currentPhoto);
            int i = bundle.getInt(MovePhoto.NEW_ALBUM_INDEX);
            albumList.get(currentAlbum).remove(currentPhoto);
            albumList.get(i).add(photo);

            albumList.store();

            //send the user back to main activity and clear the stack
            Intent back = new Intent(this, PhotoAlbum.class);
            startActivity(back.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP));
        }
    }

    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.photo_display_menu, menu);
        return super.onCreateOptionsMenu(menu);
    }

    public boolean onOptionsItemSelected(MenuItem item) {
        switch(item.getItemId()) {
            case R.id.action_edit:
                addTags();
                return true;

            case R.id.action_move:
                movePhoto();
                return true;

            case R.id.action_delete:
                deletePhoto();
                return true;

            case R.id.action_deletetags:
                deleteTags();
                return true;

            //if the back button is pressed, cancel activity
            case android.R.id.home:
                setResult(Activity.RESULT_CANCELED);
                finish();
                return true;

            default:
                return super.onOptionsItemSelected(item);
        }
    }
}
