package com.example.group68.photoalbum;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.RadioGroup;
import android.widget.RadioGroup.OnCheckedChangeListener;
import android.widget.Spinner;
import android.widget.SpinnerAdapter;
import android.widget.Toast;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.util.ArrayList;

/**
 * Created by Becca on 5/1/16.
 */
public class MovePhoto extends AppCompatActivity {

    public static final String NEW_ALBUM_INDEX = "newalbumindex";
    int currentAlbum;
    int currentPhoto;
    ArrayList<String> albums;

    private Spinner spinner;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.move_photo);

        Toolbar toolbar = (Toolbar)findViewById(R.id.my_toolbar);
        setSupportActionBar(toolbar);

        ActionBar ab = getSupportActionBar();
        ab.setDisplayHomeAsUpEnabled(true);

        Bundle bundle = getIntent().getExtras();
        currentAlbum = bundle.getInt(PhotoAlbum.ARRAY_INDEX_KEY);
        currentPhoto = bundle.getInt(OpenAlbum.CURRENT_PHOTO);
        albums = bundle.getStringArrayList(DisplayPhoto.ALBUM_LIST);

        spinner = (Spinner)findViewById(R.id.choose_album);

        spinner.setAdapter(new ArrayAdapter<String>(this, R.layout.album, albums));

        //valueText = (EditText)findViewById(R.id.tag_value);
    }

    public void move(View view){
        if(spinner.getSelectedItem() == null){
            Toast.makeText(this, "No albums", Toast.LENGTH_SHORT)
                    .show();
            return;
        }
        int selected = spinner.getSelectedItemPosition();

        // make Bundle
        Bundle bundle = new Bundle();

        bundle.putInt(PhotoAlbum.ARRAY_INDEX_KEY, currentAlbum);
        bundle.putInt(OpenAlbum.CURRENT_PHOTO, currentPhoto);
        bundle.putInt(NEW_ALBUM_INDEX, selected);

        // send back to caller
        Intent intent = new Intent();
        intent.putExtras(bundle);

        setResult(RESULT_OK, intent);
        finish(); // pops the activity from the call stack, returns to parent
    }

    public boolean onOptionsItemSelected(MenuItem item) {
        switch(item.getItemId()) {

            //if the back button is pressed, cancel activity
            case android.R.id.home:
                setResult(Activity.RESULT_CANCELED);
                finish();
                return true;

            default:
                return super.onOptionsItemSelected(item);
        }
    }
}
