package com.example.group68.photoalbum;

import android.content.Context;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.HashSet;

/**
 * Created by Becca on 4/30/2016.
 */
public class AlbumList extends ArrayList<Album> {

    private static AlbumList albumList = null;
    Context context;
    public static final String ALBUM_FILE = "albumlist.dat";

    private AlbumList() {
        super();
    }

    public static AlbumList getInstance(Context ctx) throws IOException {
        if(albumList == null){
            albumList = new AlbumList();
            albumList.context = ctx;
            albumList.load();
        }

        return albumList;
    }

    public void addAlbum(String name){
        this.add(new Album(name));
        store();
    }

    public ArrayList<String> getAlbumNameString(){

        ArrayList<String> nameList = new ArrayList<>();
        for(Album a: this){
            nameList.add(a.name);
        }

        return nameList;
    }

    public String saveString(){
        String s = "";

        for(Album a: this){
            s += a.saveString() + "\n";
        }

        return s;
    }

    public ArrayList<Photo> getFlatList(){
        ArrayList<Photo> flat = new ArrayList<Photo>();

        for(Album a: this){
            for(Photo p: a){
                flat.add(p);
            }
        }

        return flat;
    }

    public int[] search(String query){
        //if query is empty string, return empty array
        if(query == null || query.length() == 0){
            return new int[0];
        }

        ArrayList<Photo> photos = getFlatList();
        ArrayList<Integer> matches = new ArrayList<Integer>();
        String tag = query.toLowerCase();

        for(int i = 0; i < photos.size(); i++){
            for(Tag t: photos.get(i).tagList){
                if(t.value.toLowerCase().startsWith(tag)){
                    matches.add(i);
                    break;
                }
            }
        }

        int size = matches.size();
        int[] intArray = new int[size];

        for(int i = 0; i < size; i++){
            intArray[i] = matches.get(i);
        }

        return intArray;
    }

    public void setContext(Context context){
        this.context = context;
    }
    public void store() {

        try {
            FileOutputStream fos =
                    this.context.openFileOutput(ALBUM_FILE,Context.MODE_PRIVATE);
            PrintWriter pw = new PrintWriter(fos);

            pw.print(albumList.saveString());

            pw.close();
        }catch(IOException e){
            return;
        }
    }

    public void load() {
        AlbumList albums = new AlbumList();
        FileInputStream fis = null;
        BufferedReader br = null;

        try{
            fis = context.openFileInput(ALBUM_FILE);
            br = new BufferedReader(new InputStreamReader(fis));

            String line = br.readLine();

            while(line != null){
                while(line != null && line.length() > 0 && line.charAt(0) == '$'){
                    String name = line.substring(1);
                    Album a = new Album(line.substring(1).trim());
                    albums.add(a);
                    line = br.readLine();

                    while(line != null && line.length() > 0 && line.charAt(0) == '*'){
                        Photo p = new Photo(line.substring(1).trim());

                        a.addPhoto(p);
                        line = br.readLine();

                        while(line != null && line.length() > 0 && line.charAt(0) == '#'){
                            int type = Integer.parseInt(line.substring(1,2));
                            String value = line.substring(2);
                            Tag t = new Tag(type, value);

                            p.addTag(t);
                            line = br.readLine();
                        }
                    }
                }
                line = br.readLine();
            }

            br.close();
            fis.close();
        }catch(IOException e){

            return;
        }
        albumList = albums;
    }
}
